/**
 * CS402_Homework1_Part2
 * Yidan Li_A20271322
 */

import java.util.*;
import java.io.*;

public class DoubleMatrix 
{
	public static void main(String[] args) 
	{
		int M=1000, 
		    N=600, 
			H=800;
		double[][] a=new double[M][N]; //[column][row]
		double[][] b=new double[N][H];
		double[][] c=new double[M][H];

		long startTime,
			 endTime;
		int count=0;
		int countn=50;
		double totalTime=0;

		Random random=new Random();

		while (countn>0)
		{
			startTime=System.nanoTime();
		
			for (int i=0; i<M; i++)
			{
				for (int j=0; j<N; j++)
				{
					a[i][j]=random.nextDouble();
				}
			}

			for (int i=0; i<N; i++)
			{
				for (int j=0; j<H; j++)
				{
					b[i][j]=random.nextDouble();
				}
			}

			for (int i=0; i<M; i++)
			{
				for (int j=0; j<H; j++)
				{
					for (int k=0; k<N; k++)
					{
						c[i][j]+=a[i][k]*b[k][j];

					}
				}
			}

			endTime=System.nanoTime();
			totalTime += (endTime-startTime)/Math.pow(10,9);

			count++;
			countn--;
		}

		double averageTime=totalTime/count;
		System.out.println("The elapsed time is " + averageTime + " seconds.");
	}
}
